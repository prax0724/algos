package com.prax.linkedlist;

public class Item {
	
	private String itemName;
	private int itemValue;
	private Item nextItem;
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getItemValue() {
		return itemValue;
	}
	public void setItemValue(int itemValue) {
		this.itemValue = itemValue;
	}
	public Item getNextItem() {
		return nextItem;
	}
	public void setNextItem(Item nextItem) {
		this.nextItem = nextItem;
	}
	
	

}
