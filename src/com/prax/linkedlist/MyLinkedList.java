package com.prax.linkedlist;

public class MyLinkedList {

	private Item head;

	private int size;

	public MyLinkedList(Item head) {

		this.head = head;
		size++;
	}

	public void addElementLast(Item newItem) {

		Item temp = head;

		while (temp.getNextItem() != null) {

			temp = temp.getNextItem();
		}

		if (temp.getNextItem() == null) {

			temp.setNextItem(newItem);
		}

		size++;
	}

	public void removeElementLast() {

		Item temp = head;
		Item tempPrev = null;

		while (temp.getNextItem() != null) {
			tempPrev = temp;
			temp = temp.getNextItem();
		}

		tempPrev.setNextItem(null);
		temp = null;

		size--;

	}

	public void addElement(int Index) {

	}

	public void removeElement(int Index) {

	}

	public void getElement() {

	}

	public Item searchElement(Item item) {

		Item temp = head;

		while (temp != null) {

			if (temp.getItemValue() == item.getItemValue()) {
				break;
			} else {

				temp = temp.getNextItem();

			}
		}

		return temp;

	}

	public Item findMiddleElement() {

		Item firstPointer = getHead();

		Item secondPointer = getHead();

		int i = 0;

		while (firstPointer != null) {

			if (i != 0) {

				if (i % 2 == 0) {

					secondPointer = secondPointer.getNextItem();
				}

			}

			firstPointer = firstPointer.getNextItem();
			i++;
		}

		return secondPointer;
	}

	public void reverseList() {

	}

	public void displayList() {

		Item temp = head;

		while (temp.getNextItem() != null) {
			System.out.println(temp.getItemName() + "  " + temp.getItemValue());
			temp = temp.getNextItem();
		}

		if (temp.getNextItem() == null) {

			System.out.println(temp.getItemName() + "  " + temp.getItemValue());
		}

	}

	public Item getHead() {
		return head;
	}

	public void setHead(Item head) {
		this.head = head;
	}

}
