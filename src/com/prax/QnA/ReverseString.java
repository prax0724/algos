package com.prax.QnA;

public class ReverseString {

	
	static void reverseString(String s){
		
		char[] scharArray =  s.toCharArray();
		
		int wordStartIndex = scharArray.length-1;
		int wordEndIndex = scharArray.length-1;
		
		for (int i = scharArray.length-1; i >= 0; i--) {
			
			
					
			if(scharArray[i]== ' ' ){
				
				System.out.print(s.substring(wordStartIndex+1, wordEndIndex+1)+" ");
				wordEndIndex = wordStartIndex-1;
			}
			
			else if(i==0 ){
				
				System.out.print(s.substring(wordStartIndex, wordEndIndex+1)+" ");
				wordEndIndex = wordStartIndex-1;
			}
			wordStartIndex--;
			
		
		}
	}
	
	public static void reverseWords(char[] s) {
	    int i=0;
	    for(int j=0; j<s.length; j++){
	        if(s[j]==' '){
	            reverse(s, i, j-1);        
	            i=j+1;
	        }
	    }
	 
	    reverse(s, i, s.length-1);
	 
	    reverse(s, 0, s.length-1);
	}
	 
	public static void reverse(char[] s, int i, int j){
	    while(i<j){
	        char temp = s[i];
	        s[i]=s[j];
	        s[j]=temp;
	        i++;
	        j--;
	    }
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//reverseString("Sky is blue");
		
		reverseWords("Sky is blue".toCharArray());

	}

}
