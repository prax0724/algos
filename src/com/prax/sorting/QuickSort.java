package com.prax.sorting;

import java.lang.reflect.Array;
import java.util.Arrays;

public class QuickSort {
	
	public static int partition (int[] inputArray,int left,int right,int pivot){
		
	//	int pivot = inputArray[inputArray.length -1];
		
		int lowIndex = left ; int highIndex = right-1;
		
		int i = lowIndex ; int j =  right -1;
		
		while(lowIndex < highIndex){
			
			while(inputArray[lowIndex]< pivot){
				
				lowIndex++;
			}
			
			while(inputArray[highIndex] > pivot){
				
				highIndex--;
			}
			
			if(lowIndex < highIndex){
				
				int temp = inputArray[lowIndex] ;
				inputArray[lowIndex] = inputArray[highIndex];
				inputArray[highIndex] = temp;
				lowIndex++;
				highIndex--;				
			}
		}
		
		int temp = inputArray[lowIndex];
		inputArray[lowIndex] = pivot;
		inputArray[right] = temp;
		
		
		for (int i1 = 0; i1 < inputArray.length; i1++) {
			
			System.out.print(inputArray[i1] + " ");
			}
		
		System.out.println( " ");
		
		return lowIndex;
		
	}
	
	
	public static void quickSort(int[] input,int left, int right){        
		   if(right-left <= 0){
		      return;   
		   }else {
		      int pivot = input[right];
		      int partitionPoint = partition(input,left,right,pivot);
		      quickSort(input,left,partitionPoint-1);
		      quickSort(input,partitionPoint+1,right);
		   }        
		}   
	public static void main(String a[]){
        
      
        int[] input = {24,2,45,20,56,75,2,56,99,53,12,55,31};
        quickSort(input,0,input.length-1);
    }
}
