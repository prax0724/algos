package com.prax.sorting;

public class SelectionSort {

	public String[] sortStringArray(String[] inputArray) {

		for (int i = 0; i < inputArray.length; i++) {

			
			for (int j = i + 1; j < inputArray.length; j++) {
				
				if (inputArray[i].compareToIgnoreCase(inputArray[j]) == 1) {

					String temp = inputArray[j];
					inputArray[j] = inputArray[i];
					inputArray[i] = temp;
				}

			}
			
			  for (int k = 0; k < inputArray.length; k++) {
					
	        	  System.out.print(inputArray[k] + " ");
			}
			  System.out.println( " ");
		}

		return inputArray;
	}
	
	
	public String[] sortIntArray(String[] inputArray) {

		for (int i = 0; i < inputArray.length; i++) {

			
			for (int j = i + 1; j < inputArray.length; j++) {
				
				if (Integer.parseInt(inputArray[i]) > Integer.parseInt(inputArray[j])) 
						 {

					String temp = inputArray[j];
					inputArray[j] = inputArray[i];
					inputArray[i] = temp;
				}

			}
			
			  for (int k = 0; k < inputArray.length; k++) {
					
	        	  System.out.print(inputArray[k] + " ");
			}
			  System.out.println( " ");
		}

		return inputArray;
	}
	
}
