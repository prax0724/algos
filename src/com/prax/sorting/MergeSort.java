package com.prax.sorting;

public class MergeSort {

	/* merge function will merge two sorted arrays into one sorted array */
	private static void merge (String[] inputArray, String[] auxArray,int lo, int mid,int hi) {
		
		/*copy inputArray to auxArray*/
		for (int i = 0; i < inputArray.length; i++) {
			auxArray[i]=inputArray[i];
		}
		int i = lo ; int j = mid+1;
		
		for(int k=lo;k<=hi;k++){
			
			if(i>mid){
				
				inputArray[k] = auxArray[j++];
			}
			else if(j>hi){
				
				inputArray[k] = auxArray[i++];
			}
			else if(Integer.parseInt(auxArray[j]) > Integer.parseInt(auxArray[i])){
				
				inputArray[k] = auxArray[i++];
				
			}
			else if(Integer.parseInt(auxArray[j]) < Integer.parseInt(auxArray[i])){
				inputArray[k] = auxArray[j++];
						}
			else if(Integer.parseInt(auxArray[j]) == Integer.parseInt(auxArray[i])){
				inputArray[k] = auxArray[i++];
			}
		}
		
		
	}
	
	private static void sort(String[] inputArray, String[] auxArray,int lo, int hi){
		if(hi<=lo)
			return;
		int mid = lo + (hi-lo)/2;		
		sort(inputArray,auxArray,lo,mid);
		sort(inputArray,auxArray,mid+1,hi);
		merge(inputArray, auxArray, lo, mid, hi);
	}
	
	public static void sort(String[] inputArray){
		
		String[] auxArray = new String[inputArray.length];
        sort(inputArray,auxArray,0,inputArray.length-1);
		
	}
	
	public static void main(String args[]){
		
		String [] inputArray = new String[10];
		
		inputArray[0] = "1";
		inputArray[1] = "60";
		inputArray[2] = "12";
		inputArray[3] = "15";
		inputArray[4] = "20";
		inputArray[5] = "2";
		inputArray[6] = "3";
		inputArray[7] = "8";
		inputArray[8] = "13";
		inputArray[9] = "19";
		
		sort(inputArray);
		
		for (int i = 0; i < inputArray.length; i++) {
			
			System.out.print(inputArray[i] +" ");
		}
	}
	
}
