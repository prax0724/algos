package com.prax.sorting;

import java.util.Scanner;

import com.prax.unionfind.QuickUnionUF;

public class TestClientSort {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);

        while (true) {

            System.out.print("Enter sorting object separated by space ");
            String input = scanner.nextLine();
            
          String[] inputArray = input.split(" ");
          
          String[] sortedArray = new SelectionSort().sortIntArray(inputArray);
                      
          for (int i = 0; i < sortedArray.length; i++) {
			
        	  System.out.println(sortedArray[i]);
		}
          
          break;
         
        }
        scanner.close();

	}
}
