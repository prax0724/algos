package com.prax.unionfind;

import java.util.concurrent.ArrayBlockingQueue;

public class QuickFindUF {

	private int pointArray[];
	
	
	public QuickFindUF(int i){
		
		pointArray = new int[i];
		
		for(int j=0 ; j<i ; j++){
			
			pointArray[j] = j;
		}
	}
	
	public void union(int p , int q){
		
		for(int i =0; i< pointArray.length;i++){
			
			if(pointArray[i] == pointArray[q]){
				pointArray[i] = pointArray[p];
			}
		}
		
	}
	
	public boolean connected (int p , int q){
		
	return	pointArray[p] == pointArray[q];
		
	}
	
	
}
