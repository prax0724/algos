package com.prax.unionfind;

import java.util.Scanner;

public class TestClient {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);

        while (true) {

            System.out.print("Enter number of points : ");
            String input = scanner.nextLine();
            
            QuickUnionUF qf = new QuickUnionUF(Integer.parseInt(input));
            
            System.out.println(qf.connected(2, 3));
            qf.union(2, 3);

            System.out.println(qf.connected(2, 6));
            qf.union(6, 9);

            System.out.println(qf.connected(6, 3));
            qf.union(2, 9);

            System.out.println(qf.connected(6, 3));
            System.out.println(qf.connected(2, 3));
            System.out.println(qf.connected(2, 6));
            
            System.out.println(qf.root(2));
            System.out.println(qf.root(6));
            System.out.println(qf.root(3));
            
            break;
         
        }
        scanner.close();

	}

}
