package com.prax.unionfind;

public class QuickUnionUF {
	

	private int pointArray[];
	
	
	public QuickUnionUF(int i){
		
		pointArray = new int[i];
		
		for(int j=0 ; j<i ; j++){
			
			pointArray[j] = j;
		}
	}
	
	public int root(int n){
		
		
		while (pointArray[n] != n){
			
			n = pointArray[n];
		}
		
		return n;
	}
	
	public void union(int p , int q){
		int rp = root(p);
		int rq = root(q);
		
		pointArray[q] = rp;
		
	}
	
	public boolean connected (int p , int q){
		
		return	root(p) == root(q);
		
	}

}
